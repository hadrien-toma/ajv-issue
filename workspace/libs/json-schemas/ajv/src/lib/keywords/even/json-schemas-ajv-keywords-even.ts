import Ajv, { KeywordCxt, _ } from 'ajv';

export const jsonSchemasAjvKeywordsEven = ({ ajv }: { ajv: Ajv }) =>
	ajv.addKeyword({
		keyword: 'even',
		type: 'number',
		schemaType: 'boolean',
		code(cxt: KeywordCxt) {
			const { data, schema } = cxt;
			const op = schema ? _`!==` : _`===`;
			cxt.fail(_`${data} %2 ${op} 0`); // ... the only code change needed is to use `cxt.fail$data` here
		}
	});
