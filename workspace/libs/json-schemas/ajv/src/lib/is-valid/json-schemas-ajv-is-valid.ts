import Ajv, { JSONSchemaType, ValidateFunction } from 'ajv';
import addFormats from 'ajv-formats';
import { jsonSchemasAjvKeywordsEven } from '../keywords/even/json-schemas-ajv-keywords-even';

export interface TsJsonSchemasError<T> {
	instancePath: string;
	keyword: string;
	message: string;
	params: { limit?: number } & T;
	schemaPath: string;
}

export let jsonSchemasAjvInstance = new Ajv({ allErrors: true });

jsonSchemasAjvInstance = addFormats(jsonSchemasAjvInstance);

jsonSchemasAjvInstance = jsonSchemasAjvKeywordsEven({ ajv: jsonSchemasAjvInstance });

export const tsJsonSchemasMap = new Map<unknown, unknown>();

export const jsonSchemasAjvIsValid = <S, D>({ schema, value }: { schema: JSONSchemaType<S>; value: D }) => {
	const isSchemaInSet = (<Map<Record<string, unknown>, ValidateFunction<S>>>tsJsonSchemasMap).has(schema);
	if (!isSchemaInSet) {
		(<Map<Record<string, unknown>, ValidateFunction<S>>>tsJsonSchemasMap).set(schema, jsonSchemasAjvInstance.compile(schema));
	}
	const validateFunction = <ValidateFunction<S>>(<Map<Record<string, unknown>, ValidateFunction<S>>>tsJsonSchemasMap).get(schema);
	const isValid = (<Map<Record<string, unknown>, ValidateFunction<S>>>tsJsonSchemasMap).has(schema) ? validateFunction(value) : false;
	const errorArray = validateFunction.errors?.map((error) => ({ ...error, message: error.message ?? '' })) ?? [];
	return { isValid, errorArray };
};
 