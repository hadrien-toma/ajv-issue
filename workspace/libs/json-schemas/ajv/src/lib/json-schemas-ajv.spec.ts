import { jsonSchemasAjv } from './json-schemas-ajv';

describe('jsonSchemasAjv', () => {
  it('should work', () => {
    expect(jsonSchemasAjv()).toEqual('json-schemas-ajv');
  });
});
