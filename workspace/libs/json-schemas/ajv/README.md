# json-schemas-ajv

This library was generated with [Nx](https://nx.dev).

## Running unit tests

Run `nx test json-schemas-ajv` to execute the unit tests via [Jest](https://jestjs.io).
