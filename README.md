# ajv-issue

## Commands done to have this minimal reproductible example

````bash
npx create-nx-workspace@latest workspace --preset=empty --cli=nx --interactive=false --nx-cloud=false --packageManager=yarn --skipGit

cd workspace

yarn add \
	ajv \
	ajv-formats

yarn add --dev \
	@angular/cli \
	@nrwl/node \
    json

node_modules/.bin/json --in-place -f package.json -e "this.scripts['json'] = 'json';"
yarn run json --in-place -f package.json -e "this.scripts['nx'] = 'nx';"

yarn run nx generate @nrwl/node:library --directory=json-schemas --name=ajv --buildable --publishable --strict --importPath="@workspace/json-schemas-ajv" --standaloneConfig

yarn run json --in-place -f tsconfig.base.json -e "this.angularCompilerOptions = {};"
yarn run json --in-place -f tsconfig.base.json -e "this.angularCompilerOptions.fullTemplateTypeCheck = true;"
yarn run json --in-place -f tsconfig.base.json -e "this.angularCompilerOptions.strictTemplates = true;"
yarn run json --in-place -f tsconfig.base.json -e "this.angularCompilerOptions.trace = true;"

yarn run json --in-place -f tsconfig.base.json -e "this.compilerOptions['allowSyntheticDefaultImports'] = true;"
yarn run json --in-place -f tsconfig.base.json -e "this.compilerOptions['alwaysStrict'] = true;"
yarn run json --in-place -f tsconfig.base.json -e "this.compilerOptions['esModuleInterop'] = true;"
yarn run json --in-place -f tsconfig.base.json -e "this.compilerOptions['forceConsistentCasingInFileNames'] = true;"
yarn run json --in-place -f tsconfig.base.json -e "this.compilerOptions['noImplicitAny'] = true;"
yarn run json --in-place -f tsconfig.base.json -e "this.compilerOptions['noImplicitReturns'] = true;"
yarn run json --in-place -f tsconfig.base.json -e "this.compilerOptions['noUnusedLocals'] = true;"
yarn run json --in-place -f tsconfig.base.json -e "this.compilerOptions['noUnusedParameters'] = true;"
yarn run json --in-place -f tsconfig.base.json -e "this.compilerOptions['resolveJsonModule'] = true;"
yarn run json --in-place -f tsconfig.base.json -e "this.compilerOptions['strict'] = true;"
yarn run json --in-place -f tsconfig.base.json -e "this.compilerOptions['strictNullChecks'] = true;"
yarn run json --in-place -f tsconfig.base.json -e "this.compilerOptions['stripInternal'] = true;"

# Adapt workspace/libs/json-schemas/ajv/src/lib to create an isValid map for Ajv validators.

yarn run nx run json-schemas-ajv:build
````
